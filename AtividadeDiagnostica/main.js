let comentarios = [];

function exibirMusicas(cantor, imagem, biografia, musicas) {
    const sugestoes = document.getElementById('sugestoes');
    sugestoes.innerHTML = `
    <h3>${cantor}</h3>
    <div style="display: flex;">
      <div>
        <img src="${imagem}" alt="${cantor}" style="width: 200px; height: auto; margin-right: 10px;">
      </div>
      <div>
        <ul>
${musicas.map(musica => `<li><strong>
${musica.nome}:</strong> <a href="${musica.link}" target="_blank">${musica.nome}</a></li>`).join('')}
        </ul>
        <p class="biografia">${biografia}</p>
      </div>
    </div>
    <hr>
    <h4>Deixe seu comentário</h4>
    <form id="comentarioForm">
      <input type="text" id="nomeUsuario" placeholder="Seu nome" required><br>
      <textarea id="comentario" placeholder="Seu comentário" required></textarea><br>
      <button type="submit">Enviar</button>
    </form>
    <div id="comentarios">${exibirComentarios()}</div>`;
 
sugestoes.style.display = 'block';
 
document.getElementById('comentarioForm').addEventListener('submit', (event) => {
    event.preventDefault();
    const nome = document.getElementById('nomeUsuario').value;
    const comentario = document.getElementById('comentario').value;
    if (nome && comentario) {
      const novoComentario = { nome: nome, comentario: comentario };
      comentarios.push(novoComentario);
      document.getElementById('comentarios').innerHTML = exibirComentarios();
      localStorage.setItem('comentarios', JSON.stringify(comentarios));
      document.getElementById('nomeUsuario').value = '';
      document.getElementById('comentario').value = '';
    } else {
      alert('Por favor, preencha todos os campos do formulário.');
    }
  });
}

function exibirComentarios() {
    
    const comentariosSalvos = localStorage.getItem('comentarios');
    if (comentariosSalvos) {
      const comentariosArray = JSON.parse(comentariosSalvos);
      return comentariosArray.map(comentario => `<p><strong>${comentario.nome}</strong>: ${comentario.comentario}</p>`).join('');
    } else {
      return '';
    }
  }
  function curtirComentario(index) {
    comentarios[index].likes++;
    document.getElementById('comentarios').innerHTML = exibirComentarios();
    localStorage.setItem('comentarios', JSON.stringify(comentarios));
}
comentarios.push({ nome: 'Usuário', comentario: 'Este é um comentário.', likes: 0 });

document.getElementById('comentarios').innerHTML = exibirComentarios();
localStorage.setItem('comentarios', JSON.stringify(comentarios));

 function limpar(){
    document.getElementById('nome').value = '';
    document.getElementById('cantores').selectedIndex = 0;
    document.getElementById('sugestoes').innerHTML = '';
    document.getElementById('sugestoes').style.display = 'none';
 }

function exibirSugestoes() {
    const nome = document.getElementById('nome').value;
    const cantorSelecionado = document.getElementById('cantores').value;
    const sugestoes = document.getElementById('sugestoes');
    sugestoes.innerHTML = ''; 

    
    if (nome && cantorSelecionado) {
        switch (cantorSelecionado) {
            case 'Matue':
                exibirMusicas('Matue','https://i.pinimg.com/736x/1b/34/ec/1b34ecd20abaf24749d69e5744a65df1.jpg', 'Mais sobre Matue: Matheus Brasileiro Aguiar nasceu na cidade de Fortaleza em 11 de outubro de 1993. Foi criado em Hollywood, na Califórnia. Desde criança, a música sempre esteve muito presente em sua vida, tendo crescido numa família musical. Sua avó teve uma influência muito forte em seu crescimento e aprendizado, além de motivá-lo sempre a se expressar através do som. Ela acabou falecendo quando ele tinha 13 anos e isso teve um impacto muito forte na vida do artista, que passou a ficar muito mais tempo fora de casa arranjando muitos problemas. Em meio a tudo isso, a música se fez como uma amiga, e ele desenvolveu um laço muito forte com ela.',[
                    {nome: 'É sal', link:'https://youtu.be/5Z3-3qbxIN4?si=BR8SIfqidf_mnCqr'},
                    {nome: 'A morte do autotune', link:'https://youtu.be/CUFMfGu1yQc?si=kRIT3onVBtDeatCi'},
                    {nome: 'Banco', link:'https://youtu.be/60o1g1E2GBs?si=9d7JAPfVQp6P8NYu'},
                    {nome: 'Kenny G', link:'https://youtu.be/b-PhvPKgWjY?si=arusbxnXpHuPWNol'},
            
                ]);
                break;

            case 'Teto':
                exibirMusicas('Teto','https://cdns-images.dzcdn.net/images/cover/72b4096b59642ab6f41f149e3f4680cb/264x264.jpg','Mais sobre Teto: Clériton Sávio Santos Silva (Jacobina, 19 de outubro de 2001) mais conhecido pelo seu nome artístico Teto, é um rapper, cantor, compositor e violonista brasileiro, Ficou conhecido por meio das prévias de suas músicas, que foram lançadas não oficialmente em vários canais do YouTube, alcançando milhões de visualizações e ganhando fama em redes sociais como o TikTok e Instagram.',[
                    {nome: 'Groupies', link:'https://youtu.be/mV4cWyl7zYU?si=tt-LkiAg0IMrkLB2'},
                    {nome: 'Fim de semana no rio', link:'https://youtu.be/7AlAYttGnAg?si=_zddJ8gAZbyYlE3M'},
                    {nome: 'Minha vida é um filme', link:'https://youtu.be/XxE7bZlyiNw?si=_Re5u-S89QZcKDOd'},
                    {nome: 'Zumzumzum', link:'https://youtu.be/96gy9RNIXuI?si=Q3wTz33mgWoNEyj4'},
            
                ]);
                break;

            case 'Wiu':
                exibirMusicas('Wiu','https://portalpopline.com.br/wp-content/uploads/2022/07/MTV-MIAW-2022-Performances-Wiu-Creditos-Divulgacao-1-640x800.jpg' ,'Mais sobre wiu: WIU, nome artístico de Vinicius William Sales de Lima, nasceu em Fortaleza, no Ceará, em 2002. Seu interesse pelo rap e música eletrônica surgiu pela trilha sonora de jogos como GTA e FIFA Street, e na escola, participava de batalhas de rima que o ajudaram a aprimorar seus versos freestyle.',[
                    {nome: 'Coração de gelo', link:'https://youtu.be/p03BWB5SN4M?si=ao3OECMWpNar2tz9'},
                    {nome: 'Lagrimas de crocodilo', link:'https://youtu.be/rLsFLmr2r94?si=KF51GYaBMXo3uVSI'},
                    {nome: 'Ciumenta', link:'https://youtu.be/E3REgu9_efE?si=CxtKcPA_26yGZHZ9'},
                    {nome: 'Pitbull', link:'https://youtu.be/4vzEyNYZXAE?si=YVCbjuGNtg-jNyA9'},
                ]);
                break;
            case 'Pumapjl':
                exibirMusicas('Pumapjl','https://akamai.sscdn.co/uploadfile/letras/fotos/f/5/d/9/f5d950ea318087fc6414eb70f77ffa00.jpg' ,'Mais sobre Pumapjl: PUMAPJL é um dos mascarados do grupo e projeto FEBRE90s. Grupo Paulista/Carioca da selva de Concreto Brasileira. Ninguém sabe ao certo sua aparência nem idade, mas mesmo assim agrada e muito seus ouvintes.',[
                    {nome: 'Intro', link:'https://youtu.be/f7adeAYygKo?si=1eLm_Ji7v7b9mvEX'},
                    {nome: 'Zona Sul', link:'https://youtu.be/Wr9yBbfkOsg?si=MO9sl4GYVkt188u0'},
                    {nome: 'Aquelas coisas', link:'https://youtu.be/ug6-_1h8XaY?si=_EKKoaeHLjuTuyQv'},
                    {nome: 'Solta minha blusa', link:'https://youtu.be/TtLibeLGj1k?si=bgg9v_oetNOrkgFz'},
                ]);
                break;
            case 'Derek':
                exibirMusicas('Derek', 'https://pbs.twimg.com/profile_images/1026318174181048320/S79JN7ly_400x400.jpg','Mais sobre Derek: Natural de São Paulo, Derek tem apenas 22 anos e já é um dos maiores expoentes do trap brasileiro. Ele é um dos fundadores da Recayd Mob, um grupo de artistas multidisciplinares que inclui rappers e produtores. Com o grupo, Derek lançou alguns de seus maiores sucessos, como Plaqtudum e Flackjack.”.',[
                    {nome: 'TTF', link:'https://youtu.be/_urlIWLDvrA?si=Us2QoTB2ICm5xgwi'},
                    {nome: 'Scooby Doo', link:'https://youtu.be/6lzPkKzLzw8?si=7Uw9mLW4EpQ8Wml0'},
                    {nome: 'XUXA', link:'https://youtu.be/0Ta2EdomFPU?si=81Ghqro_ot82yu1D'},
                    {nome: ' Blah Blah', link:'https://youtu.be/irmSumj73IE?si=NzCbUgRHmL9_U8lt'},
                ]);
                break;
                default:
                    alert('Artista não encontrado')
        }
    } else {
        alert('Por favor, coloque seu nome e escolha um artista!');
    }
    
}